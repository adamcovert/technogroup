$(document).ready(function () {

  var scrollEl = document.querySelector('.rellax')

  if (scrollEl) {
    var rellax = new Rellax('.rellax');
  }

  var swiper = new Swiper ('.promo__slider', {
    // loop: true,
    autoplay: {
      delay: 7500,
    },
    speed: 750,
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    navigation: {
      prevEl: '.promo__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.promo__slider-navigation .slider-navigation__btn--next'
    },
    allowTouchMove: true
  });

  var swiper = new Swiper ('.catalog__slider', {
    spaceBetween: 20,
    pagination: {
      el: '.catalog__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.catalog__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.catalog__slider-navigation .slider-navigation__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      576: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 3
      },
      1200: {
        slidesPerView: 4
      }
    }
  });



  $('.counter__minus').click(function () {
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });

  $('.counter__plus').click(function () {
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
});